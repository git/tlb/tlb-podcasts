import QtQuick
import QtQuick.Layouts

import tlb.core
import "qrc:/tlb/ui/qml/templates"
/* import tlb.ui 1.0 */

Page {
    pageTitle: "Show Details"
    
    /* this should show actual details for a show. sqlite? */
    content: Item {
	id: root

	anchors.fill: parent

	Column {
	    anchors {
		fill: parent
		margins: 20
		bottomMargin: 80
	    }
	    clip: true

	    /* MVP: show basic stats of the show. */
	    Loader {
		id: l1

		anchors {
		    left: parent.left
		    right: parent.right
		}

		active: AppManager.shows.current !== null
		sourceComponent: /* ColumnLayout { */ Column {
		    spacing: 5

		    TextWrapper {
			text: "Name: " + AppManager.shows.current.title

			Layout.preferredWidth: parent.width
			width: parent.width
		    }
		    TextWrapper {
			wrapMode: Text.WordWrap
			/* the description already has paragraphs, add more for separation */
			text: "Description: <p>" + AppManager.shows.current.description + "</p>"

			width: parent.width
			/* these don't work by proxy if we want the word wrap to work */
			textMaxWidth: parent.width
			/* Layout.fillWidth: true */
			Layout.maximumWidth: parent.width
			Layout.preferredWidth: parent.width
			Layout.maximumHeight: 1000
		    }
		    TextWrapper {
			text: "Language: " + AppManager.shows.current.language

			width: parent.width
			Layout.preferredWidth: parent.width
		    }
		    TextWrapper {
			text: "Author: " + AppManager.shows.current.author

			width: parent.width
			Layout.preferredWidth: parent.width
		    }
		    TextWrapper {
			text: "Episodes: " + AppManager.shows.current.episodeCount

			width: parent.width
			Layout.preferredWidth: parent.width
		    }
		}
	    }

	    /* Summary of recent episodes */
	    
	}

	Text {
	    text: AppManager.shows.selectedName + " FETCH BY HAND"
	    visible: !l1.active
	}

	/* near the bottom, put a refresh button that downloads the feed. */
	Rectangle {
	    width: 40
	    height: 40
	    color: "purple"
	    anchors {
		bottom: root.bottom
		left: root.left
		margins: 30
	    }
	    MouseArea {
		anchors.fill: parent
		onClicked: {
		    AppManager.shows.refreshSelected()
		}
	    }
	}
    }
}
