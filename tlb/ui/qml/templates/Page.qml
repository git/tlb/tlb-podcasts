import QtQuick

import tlb.core

/*
 * This template is a full page in the system. Contains:
 *  - HEADER
 *  - CONTENT
 *  - FOOTER
 */

Rectangle {
    id: root

    border {
	color: "blue"
	width: 1
    }

    /* HEADER */
    Item {
	id: header
	anchors {
	    left: root.left
	    right: root.right
	    top: root.top
	}
	height: 30

	Text {
	    anchors.centerIn: header
	    text: "Tom's Podcasts"
	}
    }

    property alias pageTitle: pageName.text
    Text {
	id: pageName
	anchors {
	    verticalCenter: header.verticalCenter
	    left: parent.left
	    leftMargin: 10
	}
    }

    /* something just so there is a transition */
    Rectangle {
	id: sep
	height: 2
	width: root.width
	anchors {
	    top: header.bottom
	}
	border {
	    color: "black"
	    width: 1
	}
    }
    
    /* CONTENT */
    property alias content: load.sourceComponent
    Loader {
	id: load
	sourceComponent: undefined

	anchors {
	    top: sep.bottom
	    bottom: footer.top
	    left: root.left
	    right: root.right
	}
    }

    /* FOOTER */
    Item {
	id: footer
	
	anchors {
	    bottom: root.bottom
	    left: root.left
	    right: root.right
	}

	height: 30

	Rectangle {
	    height: footer.height
	    width: 140

	    anchors {
		right: middle.left
		rightMargin: 15
	    }
	    color: "black"

	    MouseArea {
		anchors.fill: parent
		onClicked: {
		    AppManager.requestBack()
		}
	    }
	}

	Rectangle {
	    id: middle
	    height: footer.height
	    width: 140

	    visible: false

	    anchors {
		horizontalCenter: footer.horizontalCenter
	    }
	    color: "gold"
	}

	Rectangle {
	    height: footer.height
	    width: 140

	    visible: false

	    anchors {
		left: middle.right
		leftMargin: 15
	    }
	    color: "red"
	}
    }
}
