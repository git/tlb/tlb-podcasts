#include <AppManager.h>

#include <stdio.h>
#include <QFile>

static AppManager *s_am = nullptr;
static Shows *s_shows = nullptr;

AppManager::AppManager(QObject* parent) :
  QObject(parent)
{
  s_am = this;
  s_shows = new Shows(dataDirectory(), this);

  connect(this, &AppManager::internalNewSource,
	  this, [this](QUrl sourceFile){
	      m_view->setSource(sourceFile);
	      currentPage = sourceFile;
	  }, Qt::QueuedConnection);
}

AppManager::~AppManager()
{

}

QString AppManager::dataDirectory(void) const
{
  return QStringLiteral("/home/tlb/dev/git/android-qt-sandbox/data/");
}

void AppManager::initializeView(QUrl url)
{
  m_view = new QQuickView();
  m_view->setFlags(Qt::Window);
  m_view->setResizeMode(QQuickView::SizeRootObjectToView);
  m_view->resize({450, 800});
  m_view->show();
  m_view->setSource(url);

  // this url is the base of the stack.
  currentPage = url;
}

AppManager *AppManager::create(QQmlEngine *qmlEngine, QJSEngine *jsEngine)
{
  QJSEngine::setObjectOwnership(s_am, QJSEngine::CppOwnership);
  return s_am;
}

QObject *AppManager::shows(void)
{
  return s_shows;
}

void AppManager::requestBack()
{
  fprintf(stderr, "[AM] received request go back\n");
  if(m_pageHistory.size() > 0){
    // we have something to go back to.
    fprintf(stderr, "[AM] -- going back to %s\n",
	    m_pageHistory.top().toString().toStdString().c_str());
    emit internalNewSource(m_pageHistory.pop());
  }else{
    fprintf(stderr, "[AM] --error, no where to run to\n");
  }
}

void AppManager::requestNewSource(QUrl sourceFile)
{
  fprintf(stderr, "[AM] received request to load source file '%s'\n",
	  sourceFile.toString().toStdString().c_str());
  if(QFile::exists(sourceFile.toString().remove("qrc"))){
    // when we go someplace new, push what we have now into the stack.
    m_pageHistory.push(currentPage);
    currentPage = sourceFile;
    emit internalNewSource(sourceFile);
  }else{
    fprintf(stderr, "[AM] --error! no such file\n");
  }
}
