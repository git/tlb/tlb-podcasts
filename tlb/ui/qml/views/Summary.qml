import QtQuick 2.0
import QtQml.XmlListModel

import tlb.core 1.0
import "qrc:/tlb/ui/qml/templates"
/* import tlb.ui 1.0 */

/* This view provides a summary of all podcasts. */

/* Sorting modes:
 *   - marked favorite
 *   - alphabetical
 *   - most recent updated
 *   - most unlistened
 *   - most recently listened
 *   - most episodes listened
 *   - least episodes listened
 *   - recently added
 *   - added longest ago
 *   - most hours available
 *   - shortest average episode
 *   - longest average episode
 */

/* with this fake data, provide a condensed list */
Page {
    pageTitle: "Summary"

    content: Item {
	id: root

	/* TODO: property holds fake data in it. */
	/* readonly property var podcastNames: [ */
	/*     "pete q", */
	/*     "tom woods", */
	/*     "liberty report", */
	/*     "bob murphy", */
	/*     "buck johnson", */
	/*     "ancient faith" */
	/* ] */

	/* Load the real data from xml */
	XmlListModel {
	    id: epModel
	    source: "file:///home/tlb/dev/git/android-qt-sandbox/example-data/collection_opml.xml"
	    query: "/opml/body/outline/outline"
	    XmlListModelRole { name: "title"
			       attributeName: "text" }
	    XmlListModelRole { name: "url"
			       attributeName: "xmlUrl" }
	}

	property bool pressDown: false
	property real curMouseX: 0.0
	property real curMouseY: 0.0
	property real jumpPos: 0.0
	/* property alias showBox: selector.visible */
	property string selectedTitle: ""
	property string selectedUrl: ""

	function _common_released() 
	{
	    if(selectedTitle !== ""){
		AppManager.shows.selectedName = selectedTitle
		AppManager.shows.selected = selectedUrl;
		AppManager.requestNewSource("qrc:/tlb/ui/qml/views/Show.qml")
	    }
	}

	Column {
	    id: col
	    
	    anchors{
		top: parent.top
		left: parent.left
		right: parent.right
		margins: 20
	    }
	    spacing: 3
	    Repeater {
		model: epModel
		Rectangle {
		    id: rec
		    
		    implicitHeight: t.height
		    width: col.width

		    property bool isHovered: {
			if(root.pressDown){
			    var pnt = root.mapToItem(rec, root.curMouseX, root.curMouseY)
			    
			    if(rec.contains(pnt)){
				return true
			    }
			}
			return false
		    }

		    onIsHoveredChanged:{
			if(isHovered){
			    var myMiddle = rec.height / 2
			    var newPoint = mapToItem(col, 0, myMiddle)
			    jumpPos = newPoint.y
			    /* showBox = true */
			    selectedTitle = title
			    selectedUrl = url
			}else if(selectedTitle === title){
			    console.error(title + " no longer hovered")
			    /* showBox = false */
			    selectedTitle = ""
			    selectedUrl = ""
			}
		    }

		    border {
			color: isHovered ? "red" : "black"
			width: 1
		    }

		    Text {
			id: t
			anchors {
			    left: parent.left
			    leftMargin: 5
			    verticalCenter: parent.verticalCenter
			}
			text: title
		    }
		}
	    }
	}

	Rectangle {
	    id: selector
	    anchors {
		right: col.right
	    }

	    /* init - can't be assed but this is magic */
	    y: jumpPos - 16
	    visible: false

	    color: "purple"
	    width: 70
	    height: 70
	    border {
		width: 1
		color: "yellow"
	    }
	}

	MouseArea {
	    anchors.fill: parent
	    onPositionChanged: (mouse)=> {
		root.curMouseX = mouse.x
		root.curMouseY = mouse.y
	    }
	    onPressed: (mouse)=> {
		root.pressDown = true
		root.curMouseX = mouse.x
		root.curMouseY = mouse.y
	    }
	    onReleased: (mouse)=> {
		console.error("main release event")

		/* at this moment, check who is in control */
		_common_released()
		
		root.pressDown = false
		root.curMouseX = mouse.x
		root.curMouseY = mouse.y
	    }
	}
    }
}
