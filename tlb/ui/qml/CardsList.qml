import QtQuick 2.0

import tlb.core 1.0
import "qrc:/tlb/ui/qml/templates"

/* Podcast App */
/*  Views: */
/*   1. Summary list of podcasts */
/*   2. Chronological order of all episodes from all podcasts */
/*   3. Searchable for all podcasts */
/*   4. View for one podcast */
/*   5. Categories */
/*   6. Online/Offline search */
/*   7. Suggestions? (from internet somehow) */
/*   8. Rich show notes if possible */
/*   9. Feeds of mixed types */

Column {
    id: cards

    spacing: 10
    
    Repeater {
	model: [ {"display": "summary",
		  "link"   : "qrc:/tlb/ui/qml/views/Summary.qml" },
		 {"display": "episode list",
		  "link"   : "qrc:/tlb/ui/qml/views/Episodes.qml"},
		 {"display": "search",
		  "link"   : "qrc:/tlb/ui/qml/views/Search.qml"},
		 {"display": "categories",
		  "link"   : "error.qml"},
		 {"display": "refresh all",
		  "link"   : "error.qml",
		  "action" : "refreshAll"} ]

	TextWrapper {
	    id: cardDelegate
	    name: modelData.display
	    
	    width: cards.width
	    height: 100
	    isTotallyCentered: true

	    text: modelData.display
	    isClickable: true
	    clickHandler: function() {
		if("error.qml" !== modelData.link){
		    AppManager.requestNewSource(modelData.link)
		}else if(modelData.action) {
		    if("refreshAll" == modelData.action){
			AppManager.shows.refreshAllFeeds()
		    }
		}
	    }
	}
    }
} 
