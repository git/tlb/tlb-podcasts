#ifndef _SHOWS_H_
#define _SHOWS_H_

#include "Show.h"

class Shows : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString selectedName READ selectedName WRITE setSelectedName NOTIFY selectedNameChanged)
  Q_PROPERTY(QString selected READ selected WRITE setSelected NOTIFY selectedChanged)
  Q_PROPERTY(QObject *current READ current NOTIFY currentChanged)

public:
  Shows(QString dataDirectory, QObject* parent);
  virtual ~Shows();

  QString selectedName(void);
  QString selected(void);
  QObject *current(void);

  Q_INVOKABLE void refreshAllFeeds(void);

public slots:
  void setSelectedName(const QString &name);
  void setSelected(const QString& rssFeedUrl);
  void refreshSelected(void);

signals:
  void selectedNameChanged(QString name);
  void selectedChanged(QString rssFeedUrl);
  void currentChanged(QObject *current);

private:
  void processByteArray(const QByteArray& ba);
  void checkSetCurrent(const QString& normalSelected);

  QString m_dataDirectory;
  QString m_selectedName;
  QString m_selectedFeed;
  QNetworkAccessManager *m_netMgr;
  QObject *m_current;
};

#endif // _SHOWS_H_
