import QtQuick
import QtQuick.Layouts

Rectangle {
    id: root

    property alias  text: t.text
    property alias  wrapMode: t.wrapMode
    property alias  isClickable: ma.enabled
    property var    clickHandler: undefined
    property string name: "unnamed"
    property int    buffer: 6
    property int    textMaxWidth
    property var    isTotallyCentered: undefined

    height: t.height + buffer
    width: t.width + buffer

    border {
        width: 1
        color: "red"
    }
    Text {
        id: t

        anchors {
            left: {
                if (typeof isTotallyCentered !== "undefined") {
                    if (isTotallyCentered) {
                        return undefined
                    }
                }
                return parent.left
            }
            leftMargin: {
                if (typeof isTotallyCentered !== "undefined") {
                    if (isTotallyCentered) {
                        return undefined
                    }
                }
                return 5
            }
	    fill: {
		if (typeof isTotallyCentered !== "undefined") {
                    if (isTotallyCentered) {
			return parent
		    }
		}
		return undefined
	    }
        }

	horizontalAlignment: {
	    if (typeof isTotallyCentered !== "undefined") {
                if (isTotallyCentered) {
                    return Text.AlignHCenter
                }
            }
	    return Text.AlignLeft
	}
	verticalAlignment: {
	    if (typeof isTotallyCentered !== "undefined") {
                if (isTotallyCentered) {
                    return Text.AlignVCenter
                }
            }
	    return Text.AlignTop
	}

        text: "unset"
        wrapMode: Text.WordWrap
        width: parent.width
    }

    function _l_on_clicked() {
        console.error("button '" + root.name + "' clicked")
        if (typeof clickHandler !== "undefined") {
            clickHandler()
        }
    }

    MouseArea {
        id: ma

        anchors.fill: parent

        enabled: false
        onClicked: _l_on_clicked()
    }
}
