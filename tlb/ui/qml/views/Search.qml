import QtQuick
import QtQuick.Layouts

import "qrc:/tlb/ui/qml/templates"
/* import tlb.ui 1.0 */

Page {
    id: root
    pageTitle: "Search (Global)"

    property var fields: [
	"title",
	"contentType", 		/* todo: what values are possible here */
	"itunesAuthor",
	"itunesOwnerName",
	"language",
	"host", 		/* todo: what is this */
	"description",	
    ]
    
    content: Item {
	id: search

	GridLayout {
	    height: 90
	    columns: 3
	    
	    anchors{
		top: parent.top
		left: parent.left
		right: parent.right
		topMargin: 5
		margins: 20
	    }

	    Repeater {
		model: root.fields

		Rectangle {
		    Layout.fillHeight: true
		    Layout.fillWidth: true
		    border {
			width: 1
			color: "red"
		    }
		    height: 30
		    Text {
			anchors {
			    verticalCenter: parent.verticalCenter
			    horizontalCenter: parent.horizontalCenter
			}
			text: modelData
		    }
		    MouseArea {
			anchors.fill: parent
			onClicked: {
			    console.error("clicked '" + modelData + "'")
			}
		    }
		}
	    }
	}
    }
}
