#ifndef _APP_MANAGER_H_
#define _APP_MANAGER_H_

#include <QObject>
#include <QUrl>

#include <QQmlEngine>
#include <QJSEngine>
#include <QQuickView>
#include <QStack>

#include "Shows.h"

class AppManager: public QObject
{
  Q_OBJECT;
  QML_ELEMENT;
  QML_SINGLETON;

  Q_PROPERTY(QObject* shows READ shows CONSTANT)

public:
  explicit AppManager(QObject* parent);
  ~AppManager() override;

  static AppManager *create(QQmlEngine *qmlEngine, QJSEngine *jsEngine);

  void initializeView(QUrl url);
  QObject *shows();
  [[nodiscard]] QString dataDirectory() const;

public slots:
  void requestNewSource(QUrl sourceFile);
  void requestBack();

signals:
  void internalNewSource(QUrl sourceFile);
  
private:
  QQuickView *m_view;
  QStack<QUrl> m_pageHistory;
  QUrl currentPage;
};

#endif	/*  _APP_MANAGER_H_ */
