#ifndef _SHOW_H_
#define _SHOW_H_

#include <QObject>
#include <QQmlEngine>
#include <QNetworkAccessManager>

struct showMetaData {
  QString normalizedName;
  QString realName;
  QString rssDescription;
  QString language;
  QString author;
  int episodeCount;
  struct {
    QString title;
    QUrl url;
    QUrl link;
  } image;
};

class Show : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString title       READ title        CONSTANT)
  Q_PROPERTY(QString description READ description  CONSTANT)
  Q_PROPERTY(QString language    READ language     CONSTANT)
  Q_PROPERTY(QString author      READ author       CONSTANT)
  Q_PROPERTY(int episodeCount    READ episodeCount CONSTANT)

public:
  Show(QObject *parent) :
    Show("INAVLID", "INVALID", "INVALID", "INVALID", -1, parent)
  {

  }
  Show(QString p_title,
       QString p_description,
       QString p_language,
       QString p_author,
       int     p_episodeCount,
       QObject *parent) :
    QObject(parent),
    m_title(p_title),
    m_language(p_language),
    m_author(p_author),
    m_episodeCount(p_episodeCount)
  {
    // wrap description with <p></p> if it doesn't have it

    if(p_description.contains("<p>")){
	m_description = p_description;
    }else{
      m_description = "<p>" + p_description + "</p>";
    }
  }
  Show(struct showMetaData p_meta, QObject *parent) :
    Show(p_meta.realName,
	 p_meta.rssDescription,
	 p_meta.language,
	 p_meta.author,
	 p_meta.episodeCount,
	 parent) {}
  virtual ~Show() {};

  QString title() const {return m_title;}
  QString description() const {return m_description;}
  QString language() const {return m_language;}
  QString author() const {return m_author;}
  int episodeCount() const {return m_episodeCount;}

private:
  QString m_title;
  QString m_description;
  QString m_language;
  QString m_author;
  int m_episodeCount;
};

#endif  // _SHOW_H_
