#include <Shows.h>

#include <QDir>
#include <QFile>
#include <QNetworkReply>
#include <QNetworkRequest>

#include <QDomDocument>
#include <utility>

Shows::Shows(QString dataDirectory, QObject *parent)
  : QObject(parent),
    m_dataDirectory(std::move(dataDirectory)),
    m_netMgr(new QNetworkAccessManager(this)),
    m_current(nullptr)
{
  connect(m_netMgr, &QNetworkAccessManager::finished,
	  this, [&](QNetworkReply *reply){
	    fprintf(stderr, "processing data...\n");
	    processByteArray(reply->readAll());
	    reply->deleteLater();
	  });
}

Shows::~Shows()
= default;

void Shows::checkSetCurrent(const QString& normalSelected)
{
  QFile cacheFile(m_dataDirectory + "/" + normalSelected + "/feed.xml");
  if(cacheFile.exists()){
    if(cacheFile.open(QIODevice::ReadOnly | QIODevice::Text)){
      processByteArray(cacheFile.readAll());
      cacheFile.close();
    }
  }else{
    if(nullptr != m_current){
      m_current->deleteLater();
    }
    m_current = nullptr;
    emit currentChanged(m_current);
  }
}

QString Shows::selectedName()
{
  return m_selectedName;
}

void Shows::setSelectedName(const QString &name)
{
  if(name != m_selectedName){ 
    m_selectedName = name;
    emit selectedNameChanged(m_selectedName);

    checkSetCurrent(m_selectedName.replace(' ', '_').toLower());
  }
}

QString Shows::selected()
{
  return m_selectedFeed;
}

void Shows::setSelected(const QString& rssFeedUrl)
{
  if(rssFeedUrl != m_selectedFeed){ 
    m_selectedFeed = rssFeedUrl;
    emit selectedChanged(m_selectedFeed);
  }
}

void Shows::processByteArray(const QByteArray& ba)
{
  QDomDocument feedDoc;
  QDomDocument::ParseResult res = feedDoc.setContent(ba);
  if(!res){
    // error
    fprintf(stderr, "[SH] RSS feed invalid. ba.size(): %lld. Error: %s\n",
	    ba.size(), res.errorMessage.toStdString().c_str());
  }else{
    // fine. get the title so we can write it out.

    struct showMetaData meta;
    
    // get the name from channel/title.
    QDomElement rootElement = feedDoc.documentElement(); // should be <rss>
    fprintf(stderr, "[SH] root element name: '%s'\n",
	    rootElement.nodeName().toStdString().c_str());
    QDomElement channelElement = rootElement.firstChildElement("channel");
    QDomElement titleElement = channelElement.firstChildElement("title");
    
    if(titleElement.isNull()){
      fprintf(stderr, "[SH] no title in feed.\n");
    }else{
      meta.realName = titleElement.text();
      meta.normalizedName = meta.realName.replace(' ', '_').toLower();
      fprintf(stderr, "[SH] rss title: '%s' (normal: %s)\n",
	      titleElement.text().toStdString().c_str(),
	      meta.normalizedName.toStdString().c_str());
    }

    QDomElement descriptionElement = channelElement.firstChildElement("description");
    if(!descriptionElement.isNull()){
      meta.rssDescription = descriptionElement.text();
      fprintf(stderr, "[SH] description: '%s'\n",
	      descriptionElement.text().toStdString().c_str());
    }else{
      fprintf(stderr, "[SH] no description set.\n");
    }

    QDomElement languageElement = channelElement.firstChildElement("language");
    if(!languageElement.isNull()){
      meta.language = languageElement.text();
      fprintf(stderr, "[SH] language: '%s'\n",
	      languageElement.text().toStdString().c_str());
    }else{
      fprintf(stderr, "[SH] no language set.\n");
    }

    QDomElement authorElement = channelElement.firstChildElement("itunes:author");
    if(!authorElement.isNull()){
      meta.author = authorElement.text();
      fprintf(stderr, "[SH] author: '%s'\n",
	      authorElement.text().toStdString().c_str());
    }else{
      fprintf(stderr, "[SH] no author set.\n");
    }

    // get the total episode count. just look for items under the channel area.
    QDomNodeList items = channelElement.elementsByTagName("item");
    fprintf(stderr, "[SH] episode count: %d\n", items.size());
    meta.episodeCount = items.size();

    if(nullptr != m_current){
      m_current->deleteLater();
    }
    m_current = new Show(meta, this);
    emit currentChanged(m_current);

    // TODO: image

    // QDomNodeList titles = rootElement.elementsByTagName("title");
    // fprintf(stderr, "title count: %d\n", titles.size());
    // for(int i = 0; i < titles.size(); i++){
    //   fprintf(stderr, " - [%3d]: '%s'\n", i,
    // 	      titles.at(i).toElement().text().toStdString().c_str());
    // 	      // titles.at(i).nodeValue().toStdString().c_str());
    // 	      // titles.at(i).toText().data().toStdString().c_str());
    // }

    // based on the podcast name, use the data directory to save the rss feed.
    QDir outDir;
    outDir.mkpath(m_dataDirectory + "/" + meta.normalizedName);

    QFile cacheFile(m_dataDirectory + "/" + meta.normalizedName + "/feed.xml");
    if(!cacheFile.open(QIODevice::WriteOnly | QIODevice::Text)){
      fprintf(stderr, "failed to open tmp file.\n");
    }else{
      cacheFile.write(ba);
      cacheFile.close();
    }
  }
}

void Shows::refreshAllFeeds()
{
  // open the xml file that holds this
#define COLLECTION_XML "/home/tlb/dev/git/android-qt-sandbox/example-data/collection_opml.xml"

  QFile collectionXml(COLLECTION_XML);
  if(!collectionXml.exists()){
    fprintf(stderr, "[SH] collection file missing, nop.\n");
    return;
  }

  // read it all and parse it
  if(!collectionXml.open(QIODevice::ReadOnly | QIODevice::Text)){
    fprintf(stderr, "[SH] collection failed to open, nop.\n");
    return;
  }

  QByteArray ba = collectionXml.readAll();
  collectionXml.close();

  QDomDocument collectionDoc;
  QDomDocument::ParseResult res = collectionDoc.setContent(ba);
  if(!res){
    fprintf(stderr, "[SH] collection file invalid, nop.\n");
    return;
  }

  // /opml/body/outline/outline
  QDomElement rootElement = collectionDoc.documentElement(); // opml
  QDomElement bodyElement = rootElement.firstChildElement("body");
  QDomElement outlineElement = bodyElement.firstChildElement("outline");
  QDomNodeList feedList = outlineElement.elementsByTagName("outline");

  fprintf(stderr, "[SH] feed count: %d\n", feedList.size());
  for(int i = 0; i < feedList.size(); i++){
    QString url = feedList.at(i).toElement().attribute("xmlUrl");
    if(QString() != url){
      fprintf(stderr, "[SH] requesting url '%s'...\n",
	      url.toStdString().c_str());
      m_netMgr->get(QNetworkRequest(QUrl(url)));
    }
  }
}

void Shows::refreshSelected()
{
  // need to set up the download thing
  m_netMgr->get(QNetworkRequest(QUrl(selected())));

  // QFile cache(m_dataDirectory + "/last-downloaded.txt");
  // if(cache.open(QIODevice::ReadOnly | QIODevice::Text)){
  //   QByteArray ba = cache.readAll();
  //   cache.close();
  //   fprintf(stderr, "[SH] processing cache file.\n");
  //   processByteArray(ba);
  // }else{
  //   fprintf(stderr, "[SH] failed to open file.\n");
  // }
}

QObject *Shows::current()
{
  return m_current;
}
