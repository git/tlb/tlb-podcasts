#include <stdio.h>
#include <stdlib.h>

#include <QGuiApplication>
#include <QQmlComponent>
#include <QQmlError>
#include <QQmlEngine>
#include <QUrl>
#include <QFont>

// #include <QtPlugin>
// Q_IMPORT_PLUGIN(tlb.core)

#include <AppManager.h>

int main(int argc, char **argv)
{
  fprintf(stderr, "qml sandbox startup\n");

  QGuiApplication app(argc, argv);

  // setup the font
  QFont font = QGuiApplication::font();
  // font.setFamily("Padauk");
  font.setFamily("Courier");  
  QGuiApplication::setFont(font);
  
  AppManager am(nullptr);
  am.initializeView(QUrl("qrc:/tlb/ui/qml/Main.qml"));

#if 0 // if using a QQuickWindow
  QQmlComponent rootComponent(view.engine(), "qrc:/tlb/ui/qml/Main.qml");
  if(rootComponent.status() != QQmlComponent::Ready){
    fprintf(stderr, "Main.qml failed to initialize!\n");
    foreach(const QQmlError &error, rootComponent.errors()){
      fprintf(stderr, " err: %s\n", error.toString().toStdString().c_str());
    }
    return EXIT_FAILURE;
  }
#endif

  return app.exec();
}
