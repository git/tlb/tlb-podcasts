import QtQuick 2.0

import "qrc:/tlb/ui/qml/templates"
/* import tlb.ui 1.0 */

/* SONY DISPLAY INFORMATION
 *  3840x1644, 643ppi (HUGE SCREEN)
 */

Page {
    content: Item {
	id: root

	CardsList {
	    id: cards
	    
	    anchors {
		margins: 20
		top: parent.top
		left: parent.left
		right: parent.right
		bottom: parent.bottom
	    }
	}
    }
}
